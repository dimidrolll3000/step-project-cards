export class Modal {
    constructor() {
      this.token = "";
    }
    errorMessage = "Incorrect username or password";

    showModal() {
        document.getElementById('button').onclick = function() {
            if (document.getElementById('log_in').style.display === "none"){
                document.getElementById('log_in').style.display = "block";
            }
        }
    }

    closeModal() {
        document.getElementById('cross').onclick = function() {
            document.getElementById('log_in').style.display = "none";
        }
    }

    async submit() {
        const email = document.getElementById('email').value;
        const password = document.getElementById('password').value;
        const token = await this.sendData(email, password);
        if(token !== this.errorMessage) {
            this.token = token;
            document.getElementById('errorMessage').style.display = "none";
            document.getElementById('log_in').style.display = "none";
            document.getElementById('button').style.display = "none";
        } else {
            document.getElementById('errorMessage').style.display = "block";
        }
    }

    async sendData(email, password) {
        return fetch("https://ajax.test-danit.com/api/v2/cards/login", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ email: email, password: password })
        })
        .then(response => response.text())
        .then(token => token)
    }

    validateForm() {
        if (this.validateEmail(document.getElementById('email').value) 
            && document.getElementById('password').value !== "") {
            document.getElementById('submit').removeAttribute("disabled");
            document.getElementById('submit').style.background = "#0082f1";
            document.getElementById('submit').style.cursor = "pointer"
        } else {
            document.getElementById('submit').setAttribute("disabled", "disabled");
            document.getElementById('submit').style.background = "#0082f180";
            document.getElementById('submit').style.cursor = "auto"
        }     
    }

    setValidate() {
        document.getElementById('email').oninput = (e) => {
            this.validateForm();
            if (this.validateEmail(e.target.value)) {
                document.getElementById('email').classList.remove("error");
            }
            else {
                document.getElementById('email').classList.add("error");
            }
        }

        document.getElementById('password').oninput = (e) => {
            this.validateForm();
            if (e.target.value !== "") {
                document.getElementById('password').classList.remove("error");
            }
            else {
                document.getElementById('password').classList.add("error");
            }
        }
    }

    validateEmail(email) {
        return email.match(
          /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        );
      };

    eyePassword() {
        const togglePassword = document.getElementById("togglePassword");
        const password = document.getElementById("password");

        togglePassword.onclick = () => {
            const type = password.getAttribute("type") === "password" ? "text" : "password";
            password.setAttribute("type", type);
            if (togglePassword.classList.contains("bi-eye-slash")){
                document.getElementById("togglePassword").className = "bi-eye";
            }
            else {
                document.getElementById("togglePassword").className = "bi bi-eye-slash"
            }
        };
    }

    init() {
        this.showModal();
        this.closeModal();
        this.setValidate();
        this.eyePassword();
    }
}