import { Modal } from './Modal.js'

export class App {
    constructor() {
        this.token = "";
    }
    modal = new Modal();
    //оголошуйте тут ваші класи

    setSubmit() {
        document.getElementById('submit').onclick = () => {
            this.getToken();
        }
    }

    async getToken() {
        await this.modal.submit();
        this.token = this.modal.token;
        //тут викликати методи в які буде передаватись токен або сеттер токена
    }
    
    init() {
        this.setSubmit();
        this.modal.init();
        //ініціалізуйте тут ваші класи
    }
}